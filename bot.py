import discord
import time
import collections

TOKEN = "NDEwMTU1MjE5MDE2NjEzODg4.XcyYDg.dHU9qg-wq8c9F7wmh01xH5oPUJ8"
BOT_PREFIX = ("%")

client = discord.Client()


@client.event
async def on_message(message):
    # we do not want the bot to reply to itself
    if message.author == client.user:
        return
    if message.content == "{}hello".format(BOT_PREFIX):
        msg = "Hello {0.author.mention}".format(message)
        await message.channel.send(msg)
    if message.content == "{}stats".format(BOT_PREFIX):
        await message.channel.send("Starting analysis of {}".format(message.channel))
        localtime = time.asctime(time.localtime(time.time()))
        embed = discord.Embed(title="Statistics", description="")
        embed.add_field(name="Time Analyzed", value=localtime)
        channel = message.channel
        msg_num = 0
        words = ""
        async for msg in channel.history(limit=None):
            msg_num += 1
            words += " " + msg.content
        #await message.channel.send(words)
        embed.add_field(name="Messages sent in this channel", value=msg_num)
        embed.add_field(name="Most common used words",
                        value="To be implemented")
        embed.add_field(name="Most common used emojis",
                        value="To be implemented")
        await message.channel.send(content=None, embed=embed)


@client.event
async def on_ready():
    print('Logged in as')
    print(client.user.name)
    print(client.user.id)
    print('------')


client.run(TOKEN)
